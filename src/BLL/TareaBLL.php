<?php

namespace App\BLL;

use App\Entity\Tarea;
use Exception;

class TareaBLL extends BaseBLL
{
    public function nueva(string $nombre)
    {
        $tarea = new Tarea();

        $tarea->setNombre($nombre);

        $user  =$this->getUser();

        $tarea->setUsuario($user);

        return $this->guardaValidando($tarea);
    }

    public function getTareasFiltradas(
        string $estado=null,
        string $prioridad=null,
        string $nombre=null,
        string $order=null)
    {
        $user = $this->getUser();

        $tareas = $this->em->getRepository(Tarea::class)
            ->getTareasFiltradas(
                $user, $estado, $prioridad, $nombre, $order);

        return $this->entitiesToArray($tareas);
    }

    public function update(Tarea $tarea, array $data)
    {
        $tarea->setNombre($data['nombre']);
        $tarea->setPrioridad($data['prioridad']);
        $tarea->setEstado($data['estado']);

        return $this->guardaValidando($tarea);
    }

    public function delete(Tarea $tarea)
    {
        $this->deleteEntity($tarea);
    }

    public function toArray($tarea)
    {
        if (is_null($tarea))
            return null;

        if (!($tarea instanceof Tarea))
            throw new Exception("La entidad no es una Tarea");

        return [
            'id' => $tarea->getId(),
            'nombre' => $tarea->getNombre(),
            'prioridad' => $tarea->getPrioridad(),
            'estado' => $tarea->getEstado(),
// 'usuario' => $tarea->getUsuario()->getUsername(),
            'fechaAlta' => $tarea->getFechaAlta()->format("d-m-Y H:i:s")
        ];
    }
}