<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TareaRepository")
 */
class Tarea
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El nombre no puede quedar vacío")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "MEDIA"})
     * @Assert\Choice(choices={
     *     "ALTA", "MEDIA", "BAJA"},
     *      message="La prioridad tiene que ser ALTA, MEDIA o BAJA")
     */
    private $prioridad;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "PENDIENTE"})
     * @Assert\Choice(choices={
     *     "PENDIENTE", "EN PROCESO", "TERMINADA"},
     *      message="El estado tiene que ser PENDIENTE, EN PROCESO o TERMINADA")
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tareas")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fechaAlta;

    public function __construct()
    {
        $this->prioridad = 'MEDIA';
        $this->estado = 'PENDIENTE';
        $this->fechaAlta = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getPrioridad(): ?string
    {
        return $this->prioridad;
    }

    public function setPrioridad(string $prioridad): self
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaAlta(): ?\DateTimeInterface
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(\DateTimeInterface $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     * @return Tarea
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }


}
