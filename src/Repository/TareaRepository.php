<?php

namespace App\Repository;

use App\Entity\Tarea;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\User;

/**
 * @method Tarea|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tarea|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tarea[]    findAll()
 * @method Tarea[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TareaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tarea::class);
    }

    public function getTareasFiltradas(
        User $user,
        string $estado=null,
        string $prioridad=null,
        string $nombre=null,
        string $order=null)
    {
        $qb = $this ->createQueryBuilder('tarea');

        $qb->where($qb->expr()->eq('tarea.usuario', $user->getId()));

        if (isset($estado))
        {
            $qb->andWhere($qb->expr()->eq('tarea.estado', ':estado'))
                ->setParameter('estado', $estado);
        }
        if (isset($prioridad))
        {
            $qb->andWhere($qb->expr()->eq('tarea.prioridad', ':prioridad'))
                ->setParameter('prioridad', $prioridad);
        }
        if (isset($nombre))
        {
            $qb->andWhere($qb->expr()->like('tarea.nombre', ':nombre'))
                ->setParameter('nombre', '%'.$nombre.'%');
        }

        $qb->orderBy('tarea.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }
}
