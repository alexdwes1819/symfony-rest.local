<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 11/02/19
 * Time: 18:21
 */

namespace App\Controller\rest;

use App\BLL\TareaBLL;
use App\Entity\Tarea;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TareaController extends BaseApiController
{
    /**
     * @Route(
     *      "/tareas.{_format}", name="post_tareas",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"POST"}
     * )
     */
    public function post(Request $request, TareaBLL $tareaBLL)
    {
        $data = $this->getContent($request);

        $tarea = $tareaBLL->nueva($data['nombre']);

        return $this->getResponse($tarea, Response::HTTP_CREATED);
    }

    /**
     * @Route("/tareas/{id}.{_format}", name="get_tarea",
     * requirements={
     *      "id": "\d+",
     *      "_format": "json"
     * },
     * defaults={"_format": "json"},
     * methods={"GET"})
     */
    public function getOne(Tarea $tarea, TareaBLL $tareaBLL)
    {
        return $this->getResponse($tareaBLL->toArray($tarea));
    }

    /**
     * @Route("/tareas.{_format}", name="get_tareas",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Route("/tareas/ordenadas/{order}", name="get_tareas_ordenadas")
     */
    public function getAll(
        Request $request,
        TareaBLL $tareaBLL,
        string $order='prioridad')
    {
        $estado = $request->query->get('estado');
        $prioridad = $request->query->get('prioridad');
        $nombre = $request->query->get('nombre');

        $tareas = $tareaBLL->getTareasFiltradas(
            $estado, $prioridad, $nombre, $order);

        return $this->getResponse($tareas);
    }

    /**
     * @Route("/tareas/{id}.{_format}", name="update_tarea",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PUT"}
     * )
     */
    public function update(Request $request, Tarea $tarea, TareaBLL $tareaBLL)
    {
        $data = $this->getContent($request);

        $tarea = $tareaBLL->update($tarea, $data);

        return $this->getResponse($tarea, Response::HTTP_OK);
    }

    /**
     * @Route("/tareas/{id}.{_format}", name="delete_tarea",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Tarea $tarea, TareaBLL $tareaBLL)
    {
        $tareaBLL->delete($tarea);

        return $this->getResponse(
            null,
            Response::HTTP_NO_CONTENT
        );
    }
}